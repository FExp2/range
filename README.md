# Sensor de distancia

<img src="img/range_sensor.jpg" alt="fig. 1" width="300"/>

## Descripción

Este módulo contiene es un [sensor de distancia infrarrojo](http://www.ti.com/lit/ds/symlink/lm393.pdf).  

## Especificaciones

El módulo tiene 3 terminales (pines) de conexión:
- A0: salida analógica
- G: conectar a GND (ground)
- +: conectar a Vcc (+5V)

## Diagrama de conexión con Arduino

<img src="img/Schematic.jpg" alt="fig. 1" width="450"/>

## Carga del firmware

Esta aplicación es muy sencilla. Lee los datos del sensor y los envía a la computadora para visualizarlos.

1. Descargar a su PC el [firmware](firmware/firmware.ino)
2. Abrir la aplicación [Arduino IDE](https://www.arduino.cc/en/software).

<img src="img/arduinoIDE_1.png" alt="fig. 1" width="300"/>

3. Abrir el código que descargó en el punto 1.
4. Conectar el Arduino a la computadora con el cable USB.
5. Seleccionar el puerto serie correspondiente al Arduino.

<img src="img/arduinoIDE_2.png" alt="fig. 1" width="300"/>

6. Seleccionar la placa Arduino que corresponde.

<img src="img/arduinoIDE_3.png" alt="fig. 1" width="300"/>

7. Cargar el firmware.
8. Abrir el *monitor serial* o el *serial plotter*

<img src="img/arduinoIDE_4.png" alt="fig. 1" width="300"/>

9. Asegurarse que la velocidad de comunicación está seteada en 9600 bauds

### AHORA PROBÁ ACERCAR Y ALEJAR LA MANO DEL SENSOR!!!
