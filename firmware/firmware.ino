//Esta funcion se ejecuta solo una vez
//al iniciarse el Arduino. Aqui se colocan
//las instrucciones que tienen que ver con la configuracion.
void setup()
{
  Serial.begin(9600); //Iniciamos el puerto serie
  //para comunicarse con la PC
  //a una velocidad de 9600 bauds.
}

void loop()
{
  int val=analogRead(A0);  //Lee el valor del conversor A/D
  //Aplica un ajuste logarítmico para convertir la señal
  //eléctrica en distancia.
  float distancia = exp(8.5841-log(val));
  //Envia el valor leido a la PC mediante puerto serie
  Serial.print(distancia);
  Serial.println(" cm");
  delay(999);
}
